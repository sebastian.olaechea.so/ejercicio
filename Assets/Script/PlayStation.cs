﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp1
{
    class PlayStation : Consola
    {
        public string nombreGPU;
        public float PotenciaCPU;

        public PlayStation(string nombre,int precio,int RAM,string CPU,string nombreGPU, float potenciaCPU) : base(nombre, precio, RAM, CPU)
        {
            this.nombreGPU = nombreGPU;
            this.PotenciaCPU = potenciaCPU;
        }
    }
}
